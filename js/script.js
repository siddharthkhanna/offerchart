// Speed of the automatic slideshow
var slideshowSpeed = 10000;

var photos = [ {
		"title" : "Stairs",
		"image" : "final_images_2/home_01.png",
		"firstline" : "",
		"secondline" : "<b style='font-size:50px'>offerchart</b> provides you thousands of downloads giving your android game visibility, discoverability and above all the chance to grow your user base. We'll gurantee you will get noticed!"
	}, {
		"title" : "Office Appartments",
		"image" : "final_images_2/home_02.png",
		"firstline" : "All in one Satisfaction:",
		"secondline" : "Over 100,000 downloads and Cheap price both at the same place. Now you dont need to talk to 10 different adnetworks."
	}, {
		"title" : "Mountainbiking",
		"image" : "final_images_2/home_03.png",
		"firstline" : "Boost your Ranking:",
		"secondline" : "Boost-up the ranking of your game/app through our service and start getting most out of your game within 3 days of launch"
	}, {
		"title" : "fourth",
		"image" : "final_images_2/home_04.png",
		"firstline" : "Maximize ROI:",
		"secondline" : "Get hundreds of thousands of organic users with micro-targeted burst campaigns"
	}
];

$(document).ready(function() {	
	$("#back").click(function() {
		stopAnimation();
		navigate("back");
	});
	
	$("#next").click(function() {
		stopAnimation();
		navigate("next");
	});
	var interval;
	$("#control").toggle(function(){
		stopAnimation();
	}, function() {
		$(this).css({ "background-image" : "url(images/btn_pause.png)" });
		navigate("next");
		interval = setInterval(function() {
			navigate("next");
		}, slideshowSpeed);
	});
	var activeContainer = 1;	
	var currentImg = 0;
	var animating = false;
	var navigate = function(direction) {
		if(animating) {
			return;
		}
		if(direction == "next") {
			currentImg++;
			if(currentImg == photos.length + 1) {
				currentImg = 1;
			}
		} else {
			currentImg--;
			if(currentImg == 0) {
				currentImg = photos.length;
			}
		}
		var currentContainer = activeContainer;
		if(activeContainer == 1) {
			activeContainer = 2;
		} else {
			activeContainer = 1;
		}
		showImage(photos[currentImg - 1], currentContainer, activeContainer);
	};
	var currentZindex = -1;
	var showImage = function(photoObject, currentContainer, activeContainer) {
		animating = true;
		currentZindex--;
		
		$("#headerimg" + activeContainer).css({
			"background-image" : "url(images/" + photoObject.image + ")",
			"display" : "block",
			"z-index" : currentZindex
		});
		
		$("#headertxt").css({"display" : "none"});
		$("#firstline").html(photoObject.firstline);
		$("#secondline")
			.html(photoObject.secondline);
		$("#pictureduri")
			.html(photoObject.title);
		$("#headerimg" + currentContainer).fadeOut(function() {
			setTimeout(function() {
				$("#headertxt").css({"display" : "block"});
				animating = false;
			}, 500);
		});
	};
	
	var stopAnimation = function() {
		$("#control").css({ "background-image" : "url(images/btn_play.png)" });
		clearInterval(interval);
	};
	
	navigate("next");
	interval = setInterval(function() {
		navigate("next");
	}, slideshowSpeed);
});